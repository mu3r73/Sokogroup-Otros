Proyecto de Sistemas Operativos en Python
(diseño: Sokogroup - fork del ahora defunct gitlab.com/CIUBel-SO-2017/Sokogroup-Otros)


Requisitos:
- Python 3.6+ (para f-strings / string interpolation)
- PySide o PyQt4 (para la UI)


---

PySide/PyQt4 en Windows:

pip install pyside/pyqt4 NO funciona (25/09/2017),
pero pip puede instalar PySide y/o PyQt4 desde un paquete wheel.

Además, PySide/PyQt4 requieren librerías del Visual C++ Redistributable for Visual Studio 2015.


Cómo instalar:

1- Descargar el wheel (no oficial)
- de PySide (http://www.lfd.uci.edu/~gohlke/pythonlibs/#pyside,
o
- de PyQt4 (http://www.lfd.uci.edu/~gohlke/pythonlibs/#pyqt4).

Ojo: bajar la versión correcta para la instalación de Windows
(win32 para sistemas de 32 bits, o win_amd64 para sistemas de 64 bits)
y para la versión de python ('cp36-cp36m' para python 3.6).

Por ejemplo, para Windows 7 de 32 bits:
PySide‑1.2.4‑cp36‑cp36m‑win32.whl
-o-
PyQt4‑4.11.4‑cp36‑cp36m‑win32.whl


2- Instalar el paquete wheel usando pip.

Por ejemplo, para instalar el paquete en forma global (para todos los usuarios):
a) abrir un command prompt en modo administrador
b) ir a la carpeta donde se descargó el paquete wheel
c) correr el comando para instalar el paquete - por ej.:
pip install PySide‑1.2.4‑cp36‑cp36m‑win32.whl
-o-
pip install PyQt4‑4.11.4‑cp36‑cp36m‑win32.whl


3- Si no ha sido instalado con anterioridad, descargar e instalar
Visual C++ Redistributable for Visual Studio 2015
(https://www.microsoft.com/en-us/download/details.aspx?id=48145)

