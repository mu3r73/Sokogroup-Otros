from usr.logger import Logger, LogCodes


class Gantt:
    def __init__(self, pcb_table):
        self.data = {}  # pid -> [gantt_char]
        self.pcb_table = pcb_table
        self.ticks = 0

    def on_tick(self):
        for (pid, gantt_char) in self.pcb_table.as_gantt_data():
            self.data[pid] = (self.data.get(pid) or f'{pid}: {" " * self.ticks}') + gantt_char
        self.on_log()
        self.ticks += 1

    def as_text(self):
        return '\n'.join([self.data.get(pid) for pid in sorted(self.data.keys())])

    def on_log(self):
        return Logger.log(LogCodes.GANTT, f'{self.as_text()}')
