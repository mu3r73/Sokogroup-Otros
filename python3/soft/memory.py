from abc import ABCMeta, abstractmethod
from operator import attrgetter

from hard.interrupt import IntCodes
from usr.logger import Logger, LogCodes


class MemoryManager(metaclass=ABCMeta):
    def __init__(self, mmu):
        self.mmu = mmu
        self.kernel = None

    def set_kernel(self, kernel):
        self.kernel = kernel

    @abstractmethod
    def set_algorithm(self, algorithm):
        pass

    def get_size(self):
        return self.mmu.get_size()

    @abstractmethod
    def switch_context_to(self, pid):
        pass

    @abstractmethod
    def on_kill(self, pid):
        pass

    @abstractmethod
    def load_arguments(self, pid, args):
        pass

    @abstractmethod
    def get_cell(self, address):
        pass

    @abstractmethod
    def set_cell(self, address, value):
        pass

    @abstractmethod
    def get_free_memory(self):
        pass

    def on_tick(self):
        self.log_status()

    def log_status(self):
        Logger.log(LogCodes.MEM_DUMP, self.as_text())

    @abstractmethod
    def get_stats(self, pid):
        pass

    @abstractmethod
    def as_text(self):
        pass

    def get_totals(self):
        return f'total: {self.mmu.get_size()}, free: {self.get_free_memory()}'


class MemoryManagerContiguous(MemoryManager):
    def __init__(self, mmu):
        super().__init__(mmu)
        self.base_addresses = {}  # pid -> base_address
        self.blocks_table = BlocksTable(self.mmu.get_size())
        self.free_space = self.mmu.get_size()
        self.allocation_algorithm = None

    def set_algorithm(self, algorithm):
        self.allocation_algorithm = algorithm

    def switch_context_to(self, pid):
        self.mmu.base_address = self.base_addresses.get(pid)

    def on_kill(self, pid):
        if pid not in self.base_addresses.keys():
            return
        self.free_block(self.base_addresses[pid])
        del(self.base_addresses[pid])

    def load_arguments(self, pid, args):
        self.mmu.base_address = self.base_addresses[pid]
        for i in range(0, len(args)):
            self.set_cell(i, args[i])

    def get_cell(self, relative_address):
        return self.mmu.get_cell(relative_address)

    def set_cell(self, relative_address, value):
        self.mmu.set_cell(relative_address, value)

    def find_free_block(self, size):
        if size > self.free_space:
            return None
        block = self.allocation_algorithm.find_free_block(self.blocks_table, size)
        if block is None:
            self.blocks_table.compact_blocks(self)

        block = self.allocation_algorithm.find_free_block(self.blocks_table, size)
        return block

    def assign_block(self, pid, base_address, size):
        self.base_addresses[pid] = base_address
        self.blocks_table.assign_block(pid, self.kernel.pcb_table.get_pcb(pid).path, base_address, size)
        self.free_space -= size
        self.log_status()

    def free_block(self, base_address):
        self.free_space += self.blocks_table.free_block(base_address)
        self.log_status()

    def shift_block_up(self, pid, target_address, size):
        base = self.mmu.base_address

        source_address = self.base_addresses[pid]
        for i in range(0, size):
            self.mmu.base_address = source_address
            c = self.mmu.get_cell(i)
            self.mmu.base_address = target_address
            self.mmu.set_cell(i, c)

        self.base_addresses[pid] = target_address

        self.mmu.base_address = base

    def get_free_memory(self):
        return self.free_space

    def as_text(self):
        return self.blocks_table.as_text()

    def get_stats(self, pid):
        if pid in self.base_addresses.keys():
            return f'{self.base_addresses.get(pid):#06x}'
        else:
            return 'na'  # not allocated


class BlocksTable:
    def __init__(self, size):
        self.table = {0: MemoryBlock(None, None, size)}  # base_address -> MemoryBlock

    def assign_block(self, pid, path, base_address, size):
        if self.table[base_address].size > size:
            self.table[base_address + size] = MemoryBlock(None, path, self.table[base_address].size - size)
        self.table[base_address].set_data(pid, path, size)

    def free_block(self, base_address):
        size = self.table[base_address].size
        self.table[base_address].owner = None
        self.merge_contiguous_free_blocks()
        return size

    def merge_contiguous_free_blocks(self):
        addresses = sorted(self.table.keys())
        for i in range(0, len(addresses)):
            block = self.table.get(addresses[i])
            if (block is not None) and (block.owner is None):
                self.try_merge_with_next_block_rec(block, i, addresses)
                return block.size

    def try_merge_with_next_block_rec(self, block, i, addresses):
        next_address = addresses[i + 1] if (i + 1) < len(addresses) else None
        next_block = self.table.get(next_address) if next_address is not None else None
        if (next_block is not None) and (next_block.owner is None):
            block.size += next_block.size
            del(self.table[next_address])
            self.try_merge_with_next_block_rec(block, i + 1, addresses)

    def compact_blocks(self, memory_manager):
        index, addresses = self.find_first_free_block(0)
        while index is not None:
            address = addresses[index]
            offset = self.table[address].size
            self.shift_all_following_blocks_up(index, offset, addresses, memory_manager)
            index, addresses = self.find_first_free_block(index + 1)
        self.merge_contiguous_free_blocks()

    def find_first_free_block(self, start_index):
        addresses = sorted(self.table.keys())
        for i in range(start_index, len(addresses)):
            address = addresses[i]
            if self.table[address].owner is None:
                return i, addresses
        # none found
        return None, None

    def shift_all_following_blocks_up(self, from_index, offset, addresses, memory_manager):
        for i in range(from_index, len(addresses) - 1):
            offset = self.shift_or_merge_block(i + 1, offset, addresses, memory_manager)

    def shift_or_merge_block(self, source_index, offset, addresses, memory_manager):
        source_address = addresses[source_index]
        if source_address not in self.table:
            return offset
        source_block = self.table[source_address]

        new_offset = offset
        if source_block.owner is None:
            new_offset = self.merge_contiguous_free_blocks()
        else:
            self.shift_block_up(source_address, source_block, offset, memory_manager)
        return new_offset

    def shift_block_up(self, source_address, source_block, offset, memory_manager):
        target_address = source_address - offset
        memory_manager.shift_block_up(source_block.owner, target_address, source_block.size)
        self.table[target_address].set_data(source_block.owner, source_block.path, source_block.size)
        del(self.table[source_address])
        self.table[target_address + source_block.size] = MemoryBlock(None, None, offset)

    def as_text(self):
        addresses = sorted(self.table.keys())
        res = [self.table.get(address).as_text(address) for address in addresses]
        return '\n'.join(res)


class MemoryBlock:
    def __init__(self, owner, path, size):
        self.owner = owner
        self.path = path
        self.size = size

    def set_data(self, owner, path, size):
        self.__init__(owner, path, size)

    def as_text(self, address):
        res = f'{address:#06x}-{(address + self.size - 1):#06x}: '
        if self.owner is not None:
            res += f'{self.owner}({self.path})'
        else:
            res += 'na'
        return res + f', s: {self.size}'


class AllocationAlgorithm(metaclass=ABCMeta):
    @abstractmethod
    def find_free_block(self, blocks_table, size):
        pass


class AllocationFirstFit(AllocationAlgorithm):
    def find_free_block(self, blocks_table, size):
        for address, block in blocks_table.table.items():
            if (block.owner is None) and (block.size >= size):
                return address

        # not found
        return None


class AllocationBestFit(AllocationAlgorithm):
    def find_free_block(self, blocks_table, size):
        min_address, min_diff = None, None
        for address, block in blocks_table.table.items():
            size_diff = block.size - size
            if (block.owner is None) and (block.size >= size)\
                    and ((min_address is None) or (size_diff < min_diff)):
                min_address, min_diff = address, size_diff

        # not found
        return min_address


class AllocationWorstFit(AllocationAlgorithm):
    def find_free_block(self, blocks_table, size):
        max_address, max_diff = None, None
        for address, block in blocks_table.table.items():
            size_diff = block.size - size
            if (block.owner is None) and (block.size >= size)\
                    and ((max_address is None) or (size_diff > max_diff)):
                max_address, max_diff = address, size_diff

        # not found
        return max_address


class MemoryManagerPaged(MemoryManager):
    def __init__(self, mmu):
        super().__init__(mmu)
        self.page_tables = {}  # pid -> PageTable
        self.frames_table = [None] * mmu.frame_count  # [FrameEntry]
        self.free_frames_count = mmu.frame_count
        self.running_pid = None
        self.cli_arguments = {}  # pid -> [argument]
        self.eviction_algorithm = None  # FrameEvictionAlgorithm

    def set_algorithm(self, algorithm):
        self.eviction_algorithm = algorithm

    def switch_context_to(self, pid):
        self.running_pid = pid

    def on_kill(self, pid):
        self.free_all_pages(pid)

    def load_arguments(self, pid, args):
        self.cli_arguments[pid] = []
        for i in range(0, len(args)):
            self.cli_arguments[pid].append(args[i])

    def get_cell(self, virtual_address):
        frame, offset = None, None
        while frame is None:
            frame, offset = self.get_frame_and_offset(virtual_address)
        self.update_access(frame)
        return self.mmu.get_cell((frame, offset))

    def set_cell(self, virtual_address, value):
        frame, offset = None, None
        while frame is None:
            frame, offset = self.get_frame_and_offset(virtual_address)
        self.update_access(frame)
        self.mmu.set_cell((frame, offset), value)

    def create_page_table(self, pid, pages_count):
        self.page_tables[pid] = PageTable(pages_count)

    def get_free_frame(self):
        free_frame = None
        if None in self.frames_table:
            free_frame = self.frames_table.index(None)
        return free_frame

    def update_access(self, frame):
        self.frames_table[frame].update_access(self.kernel.hardware.clock.ticks)

    def choose_frame_for_eviction(self):
        return self.eviction_algorithm.choose_frame_for_eviction(self.frames_table)

    def assign_frame(self, pid, page, frame):
        entry = self.page_tables.get(pid).pages[page]
        entry.set_frame(frame)

        if self.frames_table[frame] is None:
            self.frames_table[frame] = FrameEntry()
        self.frames_table[frame].set_data(pid, page, self.kernel.pcb_table.get_pcb(pid).path,
                                          self.kernel.hardware.clock.ticks)
        self.free_frames_count -= 1
        self.log_status()

    def assign_swap_block(self, pid, page, frame, block):
        self.page_tables.get(pid).pages[page].set_block(block)

        self.release_frame(frame)

    def free_all_pages(self, pid):
        for i, entry in enumerate(self.page_tables.get(pid).pages):
            self.free_page_entry(entry)
            self.page_tables.get(pid).pages[i] = None

    def free_page_entry(self, entry):
        if entry is None:
            return
        if entry.storage == PageTableEntry.IN_MEMORY:
            self.release_frame(entry.index)
        elif entry.storage == PageTableEntry.IN_SWAP:
            self.kernel.swap.release_block(entry.index)

    def release_frame(self, frame):
        self.frames_table[frame] = None
        self.free_frames_count += 1
        self.log_status()

    def get_frame_and_offset(self, virtual_address):
        page, offset = self.get_page_and_offset(virtual_address)
        frame = self.get_frame(page)
        return frame, offset

    def get_page_and_offset(self, virtual_address):
        page = virtual_address // self.mmu.frame_size
        offset = virtual_address % self.mmu.frame_size
        return page, offset

    def get_frame(self, page):
        page_table = self.page_tables.get(self.running_pid)
        if page_table.pages[page] is None:
            page_table.pages[page] = PageTableEntry()
        entry = page_table.pages[page]
        frame = None
        if entry.storage == PageTableEntry.IN_MEMORY:
            frame = entry.index
        else:
            self.mmu.irq_table.rise(IntCodes.PAGE_FAULT, self.running_pid, page, entry.storage)
        return frame

    def get_block(self, pid, page):
        entry = self.page_tables.get(pid).pages[page]
        block = None
        if entry.storage == PageTableEntry.IN_SWAP:
            block = entry.index
        return block

    def get_free_memory(self):
        return self.free_frames_count * self.mmu.frame_size

    def as_text(self):
        res = [f'f: {frame} - {entry.as_text() if entry is not None else "na"}'
               for frame, entry in enumerate(self.frames_table)]
        return '\n'.join(res)

    def page_tables_as_text(self):
        res = [f'pid: {pid}: - {self.page_tables.get(pid).as_text()}' for pid in self.page_tables.keys()]
        return '\n'.join(res)

    def get_totals(self):
        return f'{super().get_totals()}\n{self.kernel.swap.get_totals()}'

    def get_stats(self, pid):
        if pid in self.page_tables.keys():
            return f'pages: [ {(self.page_tables.get(pid)).as_text()} ]'
        else:
            return 'na'


class FrameEvictionAlgorithm(metaclass=ABCMeta):
    @abstractmethod
    def choose_frame_for_eviction(self, frames_table):
        pass


class FrameEvictionFIFO(FrameEvictionAlgorithm):
    def choose_frame_for_eviction(self, frames_table):
        frame_entry = min(frames_table, key=attrgetter('creation_tick'))
        return frames_table.index(frame_entry), frame_entry


class FrameEvictionLRU(FrameEvictionAlgorithm):
    def choose_frame_for_eviction(self, frames_table):
        frame_entry = min(frames_table, key=attrgetter('accessed_tick'))
        return frames_table.index(frame_entry), frame_entry


class FrameEntry:
    def __init__(self):
        self.pid = None
        self.path = None
        self.page = None
        self.creation_tick = None
        self.accessed_tick = None

    def set_data(self, pid, page, path, current_tick):
        self.pid = pid
        self.page = page
        self.path = path
        self.creation_tick = current_tick
        self.accessed_tick = current_tick

    def update_access(self, current_tick):
        self.accessed_tick = current_tick

    def as_text(self):
        if self.pid is None:
            return 0
        else:
            return f'{self.pid}({self.path}), p:{self.page}, ct:{self.creation_tick}, at:{self.accessed_tick}'


class PageTable:
    def __init__(self, pages_count):
        self.pages = [None] * pages_count

    def as_text(self):
        res = [f'{entry.as_text() if entry is not None else "na"}'
               for ind, entry in enumerate(self.pages)]
        return ', '.join(res)

    def is_valid_page(self, page):
        return page is not None and (page >= 0) and (page < len(self.pages))


class PageTableEntry:
    # class const
    NOT_ALLOCATED = 0
    IN_MEMORY = 1
    IN_SWAP = 2

    locations = {
        NOT_ALLOCATED: 'n',
        IN_MEMORY: 'f',
        IN_SWAP: 'b',
    }

    def __init__(self):
        self.storage = self.NOT_ALLOCATED
        self.index = None  # frame or block number

    def free(self):
        self.__init__()

    def set_frame(self, frame):
        self.storage = self.IN_MEMORY
        self.index = frame

    def set_block(self, block):
        self.storage = self.IN_SWAP
        self.index = block

    def as_text(self):
        return f'{self.locations[self.storage]}{self.index}'
