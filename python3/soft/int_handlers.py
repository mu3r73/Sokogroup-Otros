from abc import ABCMeta, abstractmethod

from soft.pcb import PCBState
from usr.logger import Logger, LogCodes


class IntHandler(metaclass=ABCMeta):
    def __init__(self, kernel, irq_table):
        self.kernel = kernel
        self.irq_table = irq_table

    def handle(self, args):
        # self.kernel.hardware.cpu.in_kernel_mode = True
        self.do_handle(args)
        # self.kernel.hardware.cpu.in_kernel_mode = False

    def on_ready(self, pcb):
        self.kernel.scheduler.schedule(pcb)

    def on_next(self):
        next_pcb = self.kernel.scheduler.choose_next_pcb()
        self.kernel.dispatcher.dispatch(next_pcb)

    @abstractmethod
    def do_handle(self, args):
        pass


# TODO maybe delete all 'lesser' handlers and implement a single FatalError that kills the process
class IntHandlerNoMemSpace(IntHandler):
    def do_handle(self, args):
        pid, path = args[0], args[1]
        Logger.log(LogCodes.HANDLER, 'NO_MEM_SPACE')
        Logger.log(LogCodes.STD_ERR, f'not enough memory space for process {pid}({path})')


class IntHandlerNoSwapSpace(IntHandler):
    def do_handle(self, args):
        path, program_size = args[0], args[1]
        Logger.log(LogCodes.HANDLER, 'NO_MEM_SPACE')
        Logger.log(LogCodes.STD_ERR, f'not enough free memory to load {path}')
        Logger.log(LogCodes.STD_ERR, f'free: {self.kernel.memory_manager.get_free_memory()} - '
                                     f'program size: {program_size}')


class IntHandlersNoSuchFile(IntHandler):
    def do_handle(self, args):
        path = args[0]
        Logger.log(LogCodes.HANDLER, 'NO_SUCH_FILE')
        Logger.log(LogCodes.STD_ERR, f'file not found: {path}')


class IntHandlerArgMismatch(IntHandler):
    def do_handle(self, args):
        path, param_count = args[0], args[1]
        Logger.log(LogCodes.HANDLER, 'ARG_MISMATCH')
        Logger.log(LogCodes.STD_ERR, f'{path} requires {param_count} arguments')
# end TODO


class IntHandlerNewProcess(IntHandler):
    def do_handle(self, args):
        path, priority, arguments = args[0], args[1], args[2]
        old_pcb = self.kernel.dispatcher.save_state()
        self.load_and_schedule(path, priority, arguments, old_pcb)

    def load_and_schedule(self, path, priority, arguments, old_pcb):
        res = self.kernel.loader.load(path, arguments)
        if res[0] is not None:
            start_at, size = res[1], res[2]
            pcb = self.kernel.pcb_table.create_pcb(path, priority, start_at, size)
            self.update_memory(pcb.pid, res[0], size)
            self.kernel.memory_manager.load_arguments(pcb.pid, arguments)
            Logger.log(LogCodes.HANDLER, f'NEW_PROCESS - path: {path}, priority: {priority}')
            Logger.log(LogCodes.HANDLER_DET, f'PCB: {pcb.as_text()}')
            self.kernel.dispatcher.load_state(old_pcb)
            self.on_ready(pcb)

    @abstractmethod
    def update_memory(self, pid, data, size):
        pass


class IntHandlerNewProcessContiguous(IntHandlerNewProcess):
    def update_memory(self, pid, base_address, size):
        self.kernel.memory_manager.assign_block(pid, base_address, size)


class IntHandlerNewProcessPaged(IntHandlerNewProcess):
    def update_memory(self, pid, pages_count, _):
        self.kernel.memory_manager.create_page_table(pid, pages_count)


class IntHandlerKillProcess(IntHandler):
    def do_handle(self, args):
        pid = None
        if len(args) > 0:
            pid = args[0]
        if (pid is not None) and (not self.kernel.dispatcher.is_running_process(pid)):
            self.kill_inactive_process(pid)
        else:
            self.kill_running_process()

    def kill_running_process(self):
        if self.kernel.dispatcher.running is None:
            return
        pid = self.kernel.dispatcher.running.pid
        self.kernel.memory_manager.on_kill(pid)
        self.kernel.dispatcher.on_kill()
        Logger.log(LogCodes.HANDLER, f'KILL_PROCESS - running pid: {pid}')
        self.on_next()

    def kill_inactive_process(self, pid):
        self.kernel.memory_manager.on_kill(pid)
        self.kernel.pcb_table.on_kill(pid)
        Logger.log(LogCodes.HANDLER, f'KILL_PROCESS - inactive pid: {pid}')


class IntHandlerProcessIOIn(IntHandler):
    def do_handle(self, args):
        dev_id, data = args[0], args[1]
        old_pcb = self.kernel.dispatcher.running
        Logger.log(LogCodes.HANDLER, f'PROCESS_IO_IN - pid: {old_pcb.pid}')
        Logger.log(LogCodes.HANDLER_DET, f'device id: {dev_id}, data: {data}')
        self.kernel.device_manager.add_job(dev_id, old_pcb, data)
        self.kernel.dispatcher.wait_for_io()
        self.on_next()


class IntHandlerProcessIOOut(IntHandler):
    def do_handle(self, args):
        dev_id = args[0]
        Logger.log(LogCodes.HANDLER, f'PROCESS_IO_OUT - dev_id: {dev_id}')
        logical_device = self.kernel.device_manager.devices[dev_id]
        pcb = logical_device.servicing
        logical_device.serve_next()
        if pcb.state != PCBState.TERMINATED:
            self.on_ready(pcb)
        if self.kernel.has_round_robin_timer():
            self.kernel.round_robin_timer.countdown += 1


class IntHandlerProcessTimeout(IntHandler):
    def do_handle(self, args):
        running_pcb = self.kernel.dispatcher.running
        if running_pcb:
            Logger.log(LogCodes.HANDLER, f'PROCESS_TIMEOUT - pid: {running_pcb.pid}')
            self.kernel.dispatcher.on_pause()
            self.kernel.scheduler.algorithm.add_to_ready(running_pcb)
            self.kernel.dispatcher.dispatch(self.kernel.scheduler.choose_next_pcb())


class IntHandlerPageFault(IntHandler):
    def do_handle(self, args):
        pid, page, storage = args[0], args[1], args[2]
        self.kernel.loader.load_page(pid, page, storage)
