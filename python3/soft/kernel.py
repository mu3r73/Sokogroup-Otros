from importlib import import_module

from hard.interrupt import IntCodes
from soft.device import LogicalDevice
from soft.scheduler import Scheduler
from soft.timer import SoftClock
from usr.logger import Logger, LogCodes


class Kernel:
    def __init__(self, hardware, pcb_table, dispatcher, shell, device_manager):
        self.hardware = hardware
        self.hardware.set_kernel(self)

        self.memory_manager = None
        self.loader = None
        self.swap = None

        self.pcb_table = pcb_table

        self.scheduler = Scheduler(self)
        self.dispatcher = dispatcher
        self.dispatcher.set_kernel(self)

        self.shell = shell
        self.shell.set_kernel(self)

        self.device_manager = device_manager
        # self.device_manager.set_kernel(self)
        self.device_manager.add_device(LogicalDevice(self.hardware.console))

        self.clock = SoftClock()
        self.hardware.clock.set_os_clock(self.clock)
        self.setup_clock()

        self.round_robin_timer = None

    def set_memory_manager(self, memory_manager_cls_name):
        memory_manager_cls = getattr(import_module('soft.memory'), memory_manager_cls_name)
        self.memory_manager = memory_manager_cls(self.hardware.mmu)
        self.memory_manager.set_kernel(self)
        self.hardware.cpu.set_memory_manager(self.memory_manager)
        Logger.log(LogCodes.SYS_INFO, f'memory manager: {memory_manager_cls_name}')

    def set_loader(self, loader_cls_name):
        loader_cls = getattr(import_module('soft.loaders'), loader_cls_name)
        self.loader = loader_cls(self.hardware.disk, self.hardware.irq_table)
        self.loader.set_kernel(self)
        Logger.log(LogCodes.SYS_INFO, f'memory loader: {loader_cls_name}')

    def set_swap(self, swap):
        self.swap = swap

    def set_scheduling_algorithm(self, algorithm_cls_name):
        algorithm_cls = getattr(import_module('soft.scheduler'), algorithm_cls_name)
        self.scheduler.set_algorithm(algorithm_cls(self))
        Logger.log(LogCodes.SYS_INFO, f'scheduling algorithm: {algorithm_cls_name}')

    def set_memory_management_algorithm(self, algorithm_cls_name):
        algorithm_cls = getattr(import_module('soft.memory'), algorithm_cls_name)
        self.memory_manager.set_algorithm(algorithm_cls())
        Logger.log(LogCodes.SYS_INFO, f'memory management algorithm: {algorithm_cls_name}')

    def has_round_robin_timer(self):
        return not (self.round_robin_timer is None)

    def set_round_robin_timer(self, round_robin_timer):
        self.round_robin_timer = round_robin_timer
        self.clock.connect(self.round_robin_timer)

    def set_irq_handler(self, irq, irq_handler_cls_name):
        self.hardware.irq_table.set_handler(irq, irq_handler_cls_name)
        Logger.log(LogCodes.SYS_INFO, f'IRQ handler: {IntCodes.inters[irq]} -> {irq_handler_cls_name}')

    def setup_clock(self):
        self.clock.connect(self.dispatcher)
        self.clock.connect(self.device_manager)
