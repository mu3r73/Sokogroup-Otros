from hard.interrupt import IntCodes
from usr.logger import Logger, LogCodes


class SoftClock:
    def __init__(self):
        self.connected = []  # connected objects require an on_tick() method

    def connect(self, process):
        self.connected.append(process)

    def on_tick(self):
        for process in self.connected:
            process.on_tick()


class RoundRobinTimer:
    def __init__(self, quantum, irq_table):
        self.irq_table = irq_table
        self.quantum = quantum
        self.countdown = self.quantum
        self.reset()
        Logger.log(LogCodes.SYS_INFO, f'round robin timer - quantum: {quantum}')

    def on_tick(self):
        self.countdown -= 1
        if self.countdown <= 0:
            self.reset()
            self.irq_table.rise(IntCodes.PROCESS_TIMEOUT)

    def reset(self):
        self.countdown = self.quantum
