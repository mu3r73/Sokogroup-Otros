#!/usr/bin/python3
import sys

try:
    from PyQt4 import QtGui
    print('using PyQt4')
except ImportError:
    from PySide import QtGui
    print('using PySide')

from hard.clock import Clock
from hard.cpu import CPU
from hard.disk import Disk
from hard.interrupt import IRQTable, IntCodes
from hard.memory import Memory
from hard.device import Console, DevCodes
from hard.hardware import Hardware

from soft.dispatcher import Dispatcher
from soft.device import DeviceManager
from soft.swap import Swap
from soft.pcb import PCBTable
from soft.shell import Shell
from soft.timer import RoundRobinTimer
from soft.kernel import Kernel

from usr.logger import Logger, LogCodes
from usr.programs import Programs
from ui_pyside_pyqt4.gui import UIConsole


def setup_system():
    config_logger()

    hardware = mk_hardware()
    add_files_to_disk(hardware.disk)

    kernel = mk_kernel(hardware)

    set_standard_irq_handlers(kernel)

    set_contiguous_memory(kernel)
    # set_paged_memory(kernel)

    # set_fcfs_scheduler(kernel)
    # set_priority_scheduler(kernel)
    # set_preemptive_priority_scheduler(kernel)
    set_round_robin_scheduler(kernel)

    hardware.clock.start_tick_loop()

    return kernel


def config_logger():
    # Logger.dont_log_cat(LogCodes.IRQ)
    Logger.dont_log_cat(LogCodes.IRQ_DET)
    Logger.dont_log_cat(LogCodes.MEM_ACC)
    Logger.dont_log_cat(LogCodes.MEM_ACC_DET)
    # Logger.dont_log_cat(LogCodes.DISK_ACC)
    # Logger.dont_log_cat(LogCodes.DISK_ACC_DET)
    Logger.dont_log_cat(LogCodes.INSTR_DET)
    Logger.dont_log_cat(LogCodes.PROGRAM_LDR)
    Logger.dont_log_cat(LogCodes.PCB_TABLE_DET)
    Logger.dont_log_cat(LogCodes.DISPATCH_DET)
    Logger.dont_log_cat(LogCodes.SCHEDULE)
    Logger.dont_log_cat(LogCodes.SCHEDULE_DET)
    Logger.dont_log_cat(LogCodes.HANDLER_DET)
    Logger.dont_log_cat(LogCodes.LOGICAL_DEV)
    Logger.dont_log_cat(LogCodes.LOGICAL_DEV_DET)
    # lessen 'noise' while debugging
    Logger.dont_log_cat(LogCodes.PCB_TABLE)


def mk_hardware():
    clock_freq = 0.3  # s
    mem_size = 48
    disk_size = 224

    memory = Memory(mem_size)
    irq_table = IRQTable()
    cpu = CPU(irq_table)
    clock = Clock(clock_freq)
    disk = Disk(disk_size, irq_table)
    console = Console(DevCodes.CONSOLE, 3, irq_table)

    return Hardware(memory, irq_table, cpu, clock, disk, console)


def add_files_to_disk(disk):
    disk.write_file('idle', Programs.prog_idle)
    disk.write_file('sum', Programs.prog_sum)
    disk.write_file('mul', Programs.prog_mul)
    disk.write_file('fibs', Programs.prog_fibs)
    disk.write_file('sum1to', Programs.prog_sum_from_1_to_n)
    disk.write_file('fact', Programs.prog_fact)


def mk_kernel(hardware):
    dispatcher = Dispatcher(hardware.clock, hardware.cpu)
    pcb_table = PCBTable(hardware.clock)
    shell = Shell(hardware.irq_table)
    device_manager = DeviceManager()

    return Kernel(hardware, pcb_table, dispatcher, shell, device_manager)


def set_standard_irq_handlers(kernel):
    kernel.set_irq_handler(IntCodes.KILL_PROCESS, 'IntHandlerKillProcess')
    kernel.set_irq_handler(IntCodes.PROCESS_IO_IN, 'IntHandlerProcessIOIn')
    kernel.set_irq_handler(IntCodes.PROCESS_IO_OUT, 'IntHandlerProcessIOOut')


def set_contiguous_memory(kernel):
    kernel.hardware.set_mmu('MMUContiguous')
    kernel.set_memory_manager('MemoryManagerContiguous')
    kernel.set_memory_management_algorithm('AllocationWorstFit')
    kernel.set_loader('MemoryLoaderContiguous')
    kernel.set_irq_handler(IntCodes.NEW_PROCESS, 'IntHandlerNewProcessContiguous')


def set_paged_memory(kernel):
    frame_size = 4
    kernel.hardware.set_mmu('MMUPaged', frame_size)
    kernel.set_memory_manager('MemoryManagerPaged')
    kernel.set_memory_management_algorithm('FrameEvictionLRU')
    kernel.set_loader('MemoryLoaderPaged')
    kernel.set_swap(Swap(kernel.hardware.mmu, kernel.hardware.disk, 2))
    kernel.set_irq_handler(IntCodes.NEW_PROCESS, 'IntHandlerNewProcessPaged')
    kernel.set_irq_handler(IntCodes.PAGE_FAULT, 'IntHandlerPageFault')


def set_fcfs_scheduler(kernel):
    kernel.set_scheduling_algorithm('SchedulingFCFS')


def set_priority_scheduler(kernel):
    kernel.set_scheduling_algorithm('SchedulingPriority')


def set_preemptive_priority_scheduler(kernel):
    kernel.set_scheduling_algorithm('SchedulingPriorityPreemptive')


def set_round_robin_scheduler(kernel):
    kernel.set_scheduling_algorithm('SchedulingFCFS')
    kernel.set_round_robin_timer(RoundRobinTimer(4, kernel.hardware.irq_table))
    kernel.set_irq_handler(IntCodes.PROCESS_TIMEOUT, 'IntHandlerProcessTimeout')


def main():
    # TODO re-check relationships between objects (currently promiscuous)
    # TODO factory with fluent interface for creation of components
    kernel = setup_system()

    app = QtGui.QApplication(sys.argv)
    # noinspection PyUnusedLocal
    console = UIConsole(kernel)
    # the var needs to be stored, so the garbage collector won't kill everything

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
