from abc import ABCMeta

from importlib import import_module

from hard.device import DevCodes
from hard.interrupt import IntCodes
from usr.logger import Logger, LogCodes


class Instruction(metaclass=ABCMeta):
    def __init__(self, address, code, cpu):
        self.cpu = cpu
        self.address = address
        self.code = code
        self.operand = None
        self.size = -1

    @staticmethod
    def decode(address, code, cpu):
        cls_name = InstrCodes.ops.get(code) or 'INV_OP'
        cls = getattr(import_module('hard.cpu_instruction_set'), cls_name)
        return cls(address, code, cpu)

    def fetch_operand(self):
        if self.size == 2:
            self.operand = self.cpu.memory_manager.get_cell(self.address + 1)

    def as_text(self, include_address):
        if include_address:
            return f'{self.address:#06x}: {InstrCodes.ops[self.code or 0]}'
            # return f'0x{self.address:0>{4}}: {InstrCodes.ops[self.code or 0]}'
        else:
            return InstrCodes.ops[self.code or 0]

    def execute(self):
        Logger.log(LogCodes.INSTR, f'{self.as_text(False)}')


# instruction set designed and (c) by Román, translated from Java by N

class InstrCodes:
    # class const
    INV_OP = 0  # invalid op

    LDA = 1  # reg_a <- <value>
    LDAm = 2  # reg_a <- mem[<address>]
    STA = 3  # mem[<address>] <- reg_a

    LDB = 4  # reg_b <- <value>
    LDBm = 5  # reg_b <- mem[<address>]
    STB = 6  # mem[<address>] <- reg_b

    ADD = 7  # reg_a <- reg_a + reg_b
    SUB = 8  # reg_a <- reg_a - reg_b

    PRNTA = 9  # print reg_a as a number

    NOP = 10  # no op
    END = 11  # rise end interrupt

    LDX = 12  # reg_x <- <value>
    LDXm = 13  # reg_x <- mem[<address>]
    STX = 14  # mem[<address>] <- reg_x
    DECX = 15  # reg_x <- reg_x -1

    JXNZ = 16  # pc <- pc - <value>

    ops = {
        INV_OP: 'INV_OP',
        LDA: 'LDA',
        LDAm: 'LDAm',
        STA: 'STA',
        LDB: 'LDB',
        LDBm: 'LDBm',
        STB: 'STB',
        ADD: 'ADD',
        SUB: 'SUB',
        PRNTA: 'PRNTA',
        NOP: 'NOP',
        END: 'END',
        LDX: 'LDX',
        LDXm: 'LDXm',
        STX: 'STX',
        DECX: 'DECX',
        JXNZ: 'JXNZ',
    }


class ADD(Instruction):
    def __init__(self, address, code, cpu):
        super().__init__(address, code, cpu)
        self.size = 1

    def execute(self):
        super().execute()
        self.cpu.reg_a += self.cpu.reg_b


class DECX(Instruction):
    def __init__(self, address, code, cpu):
        super().__init__(address, code, cpu)
        self.size = 1

    def execute(self):
        super().execute()
        self.cpu.reg_x -= 1


class END(Instruction):
    def __init__(self, address, code, cpu):
        super().__init__(address, code, cpu)
        self.size = 1

    def execute(self):
        super().execute()
        self.cpu.irq_table.rise(IntCodes.KILL_PROCESS)


class INV_OP(Instruction):
    def __init__(self, address, code, cpu):
        super().__init__(address, code, cpu)
        self.size = 1

    def execute(self):
        super().execute()
        self.cpu.irq_table.rise(IntCodes.INV_OP)


class JXNZ(Instruction):
    def __init__(self, address, code, cpu):
        super().__init__(address, code, cpu)
        self.size = 2

    def execute(self):
        super().execute()
        if self.cpu.reg_x != 0:
            self.cpu.pc += self.operand

    def as_text(self, include_address):
        return f'{super().as_text(include_address)} {self.operand}'


class LDA(Instruction):
    def __init__(self, address, code, cpu):
        super().__init__(address, code, cpu)
        self.size = 2

    def execute(self):
        super().execute()
        self.cpu.reg_a = self.operand

    def as_text(self, include_address):
        return f'{super().as_text(include_address)} {self.operand}'


class LDAm(Instruction):
    def __init__(self, address, code, cpu):
        super().__init__(address, code, cpu)
        self.size = 2

    def execute(self):
        super().execute()
        self.cpu.reg_a = self.cpu.memory_manager.get_cell(self.operand)

    def as_text(self, include_address):
        return f'{super().as_text(include_address)} [{self.operand}]'


class LDB(Instruction):
    def __init__(self, address, code, cpu):
        super().__init__(address, code, cpu)
        self.size = 2

    def execute(self):
        super().execute()
        self.cpu.reg_b = self.operand

    def as_text(self, include_address):
        return f'{super().as_text(include_address)} {self.operand}'


class LDBm(Instruction):
    def __init__(self, address, code, cpu):
        super().__init__(address, code, cpu)
        self.size = 2

    def execute(self):
        super().execute()
        self.cpu.reg_b = self.cpu.memory_manager.get_cell(self.operand)

    def as_text(self, include_address):
        return f'{super().as_text(include_address)} [{self.operand}]'


class LDX(Instruction):
    def __init__(self, address, code, cpu):
        super().__init__(address, code, cpu)
        self.size = 2

    def execute(self):
        super().execute()
        self.cpu.reg_x = self.operand

    def as_text(self, include_address):
        return f'{super().as_text(include_address)} {self.operand}'


class LDXm(Instruction):
    def __init__(self, address, code, cpu):
        super().__init__(address, code, cpu)
        self.size = 2

    def execute(self):
        super().execute()
        self.cpu.reg_x = self.cpu.memory_manager.get_cell(self.operand)

    def as_text(self, include_address):
        return f'{super().as_text(include_address)} [{self.operand}]'


class NOP(Instruction):
    def __init__(self, address, code, cpu):
        super().__init__(address, code, cpu)
        self.size = 1


class PRNTA(Instruction):
    def __init__(self, address, code, cpu):
        super().__init__(address, code, cpu)
        self.size = 1

    def execute(self):
        super().execute()
        self.cpu.irq_table.rise(IntCodes.PROCESS_IO_IN, DevCodes.CONSOLE, self.cpu.reg_a)


class STA(Instruction):
    def __init__(self, address, code, cpu):
        super().__init__(address, code, cpu)
        self.size = 2

    def execute(self):
        super().execute()
        self.cpu.memory_manager.set_cell(self.operand, self.cpu.reg_a)

    def as_text(self, include_address):
        return f'{super().as_text(include_address)} {self.operand}'


class STB(Instruction):
    def __init__(self, address, code, cpu):
        super().__init__(address, code, cpu)
        self.size = 2

    def execute(self):
        super().execute()
        self.cpu.memory_manager.set_cell(self.operand, self.cpu.reg_b)

    def as_text(self, include_address):
        return f'{super().as_text(include_address)} {self.operand}'


class STX(Instruction):
    def __init__(self, address, code, cpu):
        super().__init__(address, code, cpu)
        self.size = 2

    def execute(self):
        super().execute()
        self.cpu.memory_manager.set_cell(self.operand, self.cpu.reg_x)

    def as_text(self, include_address):
        return f'{super().as_text(include_address)} {self.operand}'


class SUB(Instruction):
    def __init__(self, address, code, cpu):
        super().__init__(address, code, cpu)
        self.size = 2

    def execute(self):
        super().execute()
        self.cpu.reg_a -= self.cpu.reg_b
