from importlib import import_module

from usr.logger import Logger, LogCodes


class Hardware:
    def __init__(self, memory, irq_table, cpu, clock, disk, console):
        self.memory = memory
        self.irq_table = irq_table
        self.mmu = None
        self.cpu = cpu
        self.clock = clock
        self.disk = disk
        self.console = console
        self.kernel = None

    def set_mmu(self, mmu_cls_name, frame_size=None):
        mmu_cls = getattr(import_module('hard.mmu'), mmu_cls_name)
        self.mmu = mmu_cls(self.memory, self.irq_table, frame_size)
        txt = f'MMU: {mmu_cls_name}'
        if frame_size is not None:
            txt += f', frame size: {frame_size}'
        Logger.log(LogCodes.SYS_INFO, txt)

    def set_kernel(self, kernel):
        self.kernel = kernel
        self.irq_table.set_kernel(kernel)
        self.clock.set_hardware(self)

    def on_tick(self):
        self.cpu.on_tick()
