from importlib import import_module

from usr.logger import Logger, LogCodes


class IntCodes:
    # class const
    INV_OP = 0
    INV_ADDRESS = 1
    NO_MEM_SPACE = 2
    NO_DISK_SPACE = 3
    NO_SUCH_FILE = 4
    ARG_MISMATCH = 5
    NEW_PROCESS = 6
    KILL_PROCESS = 7
    PROCESS_IO_IN = 8
    PROCESS_IO_OUT = 9
    PROCESS_TIMEOUT = 10
    PAGE_FAULT = 11
    NO_SWAP_SPACE = 12

    inters = {
        INV_OP: 'INV_OP',
        INV_ADDRESS: 'INV_ADDRESS',
        NO_MEM_SPACE: 'NO_MEM_SPACE',
        NO_DISK_SPACE: 'NO_DISK_SPACE',
        NO_SUCH_FILE: 'NO_SUCH_FILE',
        ARG_MISMATCH: 'ARG_MISMATCH',
        NEW_PROCESS: 'NEW_PROCESS',
        KILL_PROCESS: 'KILL_PROCESS',
        PROCESS_IO_IN: 'PROCESS_IO_IN',
        PROCESS_IO_OUT: 'PROCESS_IO_OUT',
        PROCESS_TIMEOUT: 'PROCESS_TIMEOUT',
        PAGE_FAULT: 'PAGE_FAULT',
        NO_SWAP_SPACE: 'NO_SWAP_SPACE',
    }


class IRQTable:
    handler_names = {
        # default handlers
        IntCodes.NO_MEM_SPACE: 'IntHandlerNoMemSpace',
        IntCodes.NO_SWAP_SPACE: 'IntHandlerNoSwapSpace',
        IntCodes.NO_SUCH_FILE: 'IntHandlersNoSuchFile',
        IntCodes.ARG_MISMATCH: 'IntHandlerArgMismatch',
    }

    def __init__(self):
        self.kernel = None

    def set_kernel(self, kernel):
        self.kernel = kernel

    def set_handler(self, irq, cls_name):
        self.handler_names[irq] = cls_name

    def rise(self, irq, *args):
        Logger.log(LogCodes.IRQ, f'{IntCodes.inters[irq]}')
        Logger.log(LogCodes.IRQ_DET, f'args: {args}')
        cls_name = self.handler_names.get(irq)
        if cls_name:
            cls = getattr(import_module('soft.int_handlers'), cls_name)
            return cls(self.kernel, self).handle(args)
