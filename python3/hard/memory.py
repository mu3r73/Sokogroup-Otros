from usr.logger import Logger, LogCodes


class Memory:
    def __init__(self, size):
        self.size = size
        self.memory = [None] * size
        Logger.log(LogCodes.SYS_INFO, f'memory size: {size}')

    def get_cell(self, address):
        return self.memory[address]

    def set_cell(self, address, value):
        self.memory[address] = value
